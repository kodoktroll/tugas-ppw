from django import forms

class Status_Form(forms.Form):
    error_messages = {
        'required' : 'Status can\'t be blank'

    }
    status_attrs = {
        'type': 'text',
        'cols': 50,
        'rows': 4,
        'class': 'todo-form-textarea',
        'placeholder':'What\'s happening?'
    }

    new_status = forms.CharField(label='', required=True, max_length=280, widget=forms.Textarea(attrs=status_attrs))
