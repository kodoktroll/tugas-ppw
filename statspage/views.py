from django.shortcuts import render
from profilepage.views import nama
from statuspage.models import Status
from add_friends.models import AddFriends


# Create your views here.
response = {}
def index(request):
	friend_count = AddFriends.objects.all().count()
	posts_count = Status.objects.all().count()

	response['nama'] = nama
	response['friend_count'] = friend_count
	response['posts_count'] = posts_count
	if(posts_count != 0):
		latest_status = Status.objects.latest('created_date')
		response['latest_status'] = latest_status
	return render(request, 'statspage.html', response)