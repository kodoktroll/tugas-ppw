from django.apps import AppConfig


class StatspageConfig(AppConfig):
    name = 'statspage'
