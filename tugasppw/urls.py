"""tugasppw URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import profilepage.urls as profilepage
import statuspage.urls as statuspage
import statspage.urls as statspage
import add_friends.urls as add_friends
from django.views.generic import RedirectView


urlpatterns = [
        url(r'^admin/', admin.site.urls),
        url(r'^$', RedirectView.as_view(
            url='/status/', permanent=True)),
        url(r'^profile/', include(profilepage, namespace='Profile Page')),
        url(r'^status/',include(statuspage, namespace='Status Page')),
		url(r'^add-friends/', include(add_friends,namespace='Friends Page')),
        url(r'^stats/',include(statspage, namespace='Stats Page')),
        url(r'^$', RedirectView.as_view(url='/profile/', permanent='true'))
]
        

