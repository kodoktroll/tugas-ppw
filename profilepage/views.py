from django.shortcuts import render
from .models import Database

# Create your views here.
Expertise = ['a', 'b', 'c', 'd', 'e', 'f']
nama = "John Doe"

response = {}


def index(request):

    response['name'] = "John Doe"
    response['birthday'] = "1 Januari 2010"
    response['sex'] = "Male"
    response['description'] = "Orang baik yang senan mengaji"
    response['email'] = "jon@ui.ac.id"
    response['expertise'] = Expertise
    return render(request, 'profilepage.html', response)
