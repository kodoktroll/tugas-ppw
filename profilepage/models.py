from django.db import models

# Create your models here.


class Database(models.Model):
    name = models.CharField(max_length=30)
    birthday = models.CharField(max_length=30)
    sex = models.CharField(max_length=7)
    description = models.TextField(max_length=200)
    email = models.TextField(max_length=40)
    expertise = models.TextField(max_length=None)
