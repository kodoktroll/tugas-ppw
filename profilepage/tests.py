from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from .models import Database


# Create your tests here.


class ProfilePageUnitTest(TestCase):

    def test_profile_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_html_is_good(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode("utf-8")

        self.assertIn("John Doe", html_response)
        self.assertIn("1 Januari 2010", html_response)
        self.assertIn("Male", html_response)
        self.assertIn("Orang baik yang senan mengaji", html_response)
        self.assertIn("jon@ui.ac.id", html_response)

    def test_expertise_item(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode("utf-8")

        self.assertIn("a", html_response)
        self.assertIn("b", html_response)
        self.assertIn("c", html_response)
        self.assertIn("d", html_response)
        self.assertIn("e", html_response)
