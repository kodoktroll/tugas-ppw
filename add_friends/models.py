from django.db import models

# Create your models here.
class AddFriends(models.Model):
    name = models.CharField(max_length=27)
    url = models.URLField()
    added_date = models.DateTimeField(auto_now=True)
    