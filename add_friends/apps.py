from django.apps import AppConfig


class AddFriendsConfig(AppConfig):
    name = 'add_friends'
