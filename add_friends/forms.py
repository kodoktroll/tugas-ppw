from django import forms

name_attrs = {
    'type': 'text',
    'class': 'add-friends-input-name',
    'placeholder':' Add by username',
}
url_attrs = {
    'class' : 'add-friends-input-url',
    'placeholder':' Insert url here',
}

class AddFriendsForm(forms.Form):
	name = forms.CharField(label="Name", required = True , max_length = 27, widget = forms.TextInput(attrs = name_attrs))
	url = forms.URLField(label="URLs", required = True, widget = forms.URLInput(attrs=url_attrs))
